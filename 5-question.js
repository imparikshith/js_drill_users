const languages = ["Golang", "Python", "Javascript"];
function groupByProgLang(users) {
  const groupedUsers = Object.entries(users).reduce(function (acc, user) {
    for (let language of languages) {
      if (user[1].desgination.includes(language)) {
        if (!acc[language]) {
          acc[language] = [];
        }
        acc[language].push(user[0]);
        break;
      }
    }
    return acc;
  }, {});
  return groupedUsers;
}
module.exports = groupByProgLang;
function usersWithMasters(users) {
  const usersWithMastersDegree = Object.entries(users).reduce(function (
    acc,
    user
  ) {
    if (user[1].qualification.includes("Masters")) {
      acc.push(user[0]);
    }
    return acc;
  },
  []);
  return usersWithMastersDegree;
}
module.exports = usersWithMasters;
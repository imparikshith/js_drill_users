function seniorityOrder(desgination) {
  if (desgination.includes("Senior") && desgination.includes("Developer")) {
    return 1;
  } else if (desgination.includes("Developer")) {
    return 2;
  } else if (desgination.includes("Intern")) {
    return 3;
  } else {
    return null;
  }
}
function sortBySeniority(obj) {
  const sortedBySeniority = Object.entries(obj)
    .sort(function (a, b) {
      if (
        seniorityOrder(a[1].desgination) == seniorityOrder(b[1].desgination)
      ) {
        return b[1].age - a[1].age;
      } else {
        return (
          seniorityOrder(a[1].desgination) - seniorityOrder(b[1].desgination)
        );
      }
    })
    .reduce(function (acc, employee) {
      acc.push(employee[0]);
      return acc;
    }, []);
  return sortedBySeniority;
}
module.exports = sortBySeniority;
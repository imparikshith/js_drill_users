function totalUsersInterestedInVideoGames(users) {
  const usersInterestedInVideoGames = [];
  for (let user in users) {
    for (let interest of users[user].interests) {
      if (interest.toLowerCase().includes("video games")) {
        usersInterestedInVideoGames.push(user);
      }
    }
  }
  return usersInterestedInVideoGames;
}
module.exports = totalUsersInterestedInVideoGames;
function totalUsersInGermany(users) {
  const usersInGermany = [];
  for (let user in users) {
    if (users[user].nationality.includes("Germany")) {
      usersInGermany.push(user);
    }
  }
  return usersInGermany;
}
module.exports = totalUsersInGermany;